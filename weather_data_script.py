#!/usr/bin/python3

from araweather import weather
import json


weather_data = weather.get_current_weather(['WilsonHall', 'AgronomyFarm'])
weather_data = json.dumps(weather_data, indent=4)

print(weather_data)
