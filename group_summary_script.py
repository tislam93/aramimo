#!/usr/bin/python3

"""
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

# This script records the grouping summary for all 36 bands and save the ouptup to json files.
# Users need to provide the number of reps while running the script. The reps are the number \\
# of files that will be generated each after every 5 seconds with the users grouping summary.

# To Run the Script
# ./group_summary_scrip.py <number of repetitions>

import os
import sys
import time
import json
from datetime import datetime
import getpass
from sklk_sonic.client import JSONZClient

# Port number
PORT = 5556

# Connect to CU
client = JSONZClient("Mu2 Control", url="tcp://10.189.0.134:" + str(PORT))

# Timestamp for file
ts_dir = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")

# Initialize
reps = int(sys.argv[1])

# Retrieve Frequency and BW in operation
connections = client.gyro_get_connections()
time.sleep(0.5)
config = client.get_config()
bandwidth = config[1]["radio_config"]["filter_bw"]
frequency = config[1]["radio_config"]["frequency"]

# Directory where the files are stored
PARENT_DIR = f"/home/{getpass.getuser()}/sklk_data"
if not os.path.exists(PARENT_DIR):
    os.makedirs(PARENT_DIR)
DIR = f"group_summary_{frequency}_{bandwidth}_{ts_dir}"
GROUP_DIR = f"{PARENT_DIR}/group_summary_data"
# Create direcory if not, to save the csi data
if not os.path.exists(GROUP_DIR):
    os.makedirs(GROUP_DIR)
PATH = os.path.join(GROUP_DIR, DIR)
os.mkdir(PATH)
time.sleep(0.5)
# Collect data
for i in range(reps):
    ts_file = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
    group_summary = client.gyro_get_group_summary(-1)
    group_summary.append(connections)
    data = json.dumps(group_summary, indent=4)
    # Save to file
    print(f"Generating file sample_{i+1}")
    with open(f'{GROUP_DIR}/{DIR}/group_summary_sample_{i+1}_{frequency}_{bandwidth}_{ts_file}.json', 'w') as fp:
        fp.write(data)
    time.sleep(5)
print(f"Group Summary Data Saved in {GROUP_DIR}")
