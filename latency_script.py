#!/usr/bin/python3

"""
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


# This script records the latency of the UEs from the BS. User needs to provide the \\
# number of reps (seconds) for which the latency will be recorded for each UE.

# To Run the Script
# ./latency_script.py <number of repetitions>

import sys
import os
import json
from datetime import datetime
import time
import getpass
import requests
from sklk_sonic.client import JSONZClient

# Port number
PORT = 5556

# Connect to CU
client = JSONZClient("Mu2 Control", url="tcp://10.189.0.134:" + str(PORT))

# Timestamp for file
ts_file = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")

# Initialize
data = {}
reps = int(sys.argv[1])

# Directory where the files are stored
PARENT_DIR = f"/home/{getpass.getuser()}/sklk_data"
if not os.path.exists(PARENT_DIR):
    os.makedirs(PARENT_DIR)
LATENCY_DIR = f"{PARENT_DIR}/latency_data"
# Create direcory if not, to save the csi data
if not os.path.exists(LATENCY_DIR):
    os.makedirs(LATENCY_DIR)

# Retrieve Frequency and BW in operation
config = client.get_config()
bandwidth = config[1]["radio_config"]["filter_bw"]
frequency = config[1]["radio_config"]["frequency"]

# Generate timestamps
ts_file = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")

# Collect data
data = {}
for i in range(reps):
    data_temp = {}
    data_temp["timestamp"] = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
    output = (requests.get('http://10.189.0.134:8000/latency_all_ues')).json()
    print(f"Generating Sample {i+1}")
    data_temp.update(output)
    data[f"Sample_{i+1}"] = data_temp
    time.sleep(1)

# Cast to JSON format
data = json.dumps(data, indent=4)
# Save to file
with open(f'{LATENCY_DIR}/latency_stats_{frequency}_{bandwidth}_{ts_file}.json', 'w') as fp:
    fp.write(data)

print(F"Latency data captured!")
print(f"Group Summary Data Saved in {LATENCY_DIR}")
