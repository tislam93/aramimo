<!-- """
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
""" -->


# AraMIMO

AraMIMO is a commercial mMIMO experimental platform as part of ARA wireless living lab at Iowa State University. AraMIMO base station (BS) is deployed at the rooftop of Wilson residence hall in phase-I of ARA. The BS hardware is developed by Skylark Wireless and it consists of CU, DU and three RUs covering three sectors. Each RU has 14 antennas. There are a total of six UEs deployed so far and more are being deployed. This BS will have a total of 20 UEs spread across the Ames area covering Ag farms, city facilities and live stock farms. 

## Getting started

This repo is an open source library for running experiments on AraMIMO platform. The decription of scripts is written at the top of each script. To run the scripts, users need to launch a pre-defined container and the steps are documented in ARA users manual. 


## How to Use

To use the scripts for experimental studies, example experiments are defined in the ARA users manual, the link of which is provided below. 
https://arawireless.readthedocs.io/en/latest/ara_experiments/ara_ran_experiments.html#experiment-2-monitoring-skylark-mimo-wireless-link-behavior





