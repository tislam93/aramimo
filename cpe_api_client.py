import cmd2
import json
import requests
from cmd2 import Cmd
import argparse

url = 'http://10.188.32.164:8002/'

welcome_banner = requests.get(url)
print(str(welcome_banner.content.decode()))
# print(response.content)
# print(response1.content)

class Prompt(Cmd):
    def __init__(self):
        super().__init__()
        self.hidden_commands += ['alias', 'run_script', 'set', 'edit', 'macro', 'shell', 'shortcuts', 'run_pyscript']

    set_tx_gain_parser = argparse.ArgumentParser()
    set_rx_gain_parser = argparse.ArgumentParser()
    get_config_parser = argparse.ArgumentParser()
    get_phy_stats_parser = argparse.ArgumentParser()
    get_conn_uptime_parser = argparse.ArgumentParser()
    get_conn_status_parser = argparse.ArgumentParser()
    set_close_radio_parser = argparse.ArgumentParser()
    set_configure_radio_parser = argparse.ArgumentParser()
    get_captured_symbol_keys_parser = argparse.ArgumentParser()
    get_captured_symbols_parser = argparse.ArgumentParser()
    capture_next_symbol_parser = argparse.ArgumentParser()
    get_captured_symbol_parser = argparse.ArgumentParser()
    capture_symbols_parser = argparse.ArgumentParser()
    get_rx_power_parser = argparse.ArgumentParser()
    get_rx_gain_parser = argparse.ArgumentParser()
    get_tx_gain_parser = argparse.ArgumentParser()    

    get_config_parser.add_argument(
        '-f',
        '--file',
        action='append',
        help="Save to File"
    )
    
    get_phy_stats_parser.add_argument(
        '-f',
        '--file',
        action='append',
        help="Save to File"
    )
    
    get_rx_power_parser.add_argument(
        '-f',
        '--file',
        action='append',
        help="Save to File"
    )

    get_rx_gain_parser.add_argument(
        '-f',
        '--file',
        action='append',
        help="Save to File"
    )

    get_tx_gain_parser.add_argument(
        '-f',
        '--file',
        action='append',
        help="Save to File"
    )

    get_conn_uptime_parser.add_argument(
        '-f',
        '--file',
        action='append',
        help="Save to File"
    )    

    get_conn_status_parser.add_argument(
        '-f',
        '--file',
        action='append',
        help="Save to File"
    )

    # set_close_radio_parser.add_argument(
        
    #     help=""
    # )

    set_tx_gain_parser.add_argument(
        '-g',
        '--txgain',
        type=float,
        help="Tx Gain value in dB"
    )

    set_rx_gain_parser.add_argument(
        '-g',
        '--rxgain',
        type=float,
        help="Rx Gain Value in dB"
    )

    get_captured_symbol_keys_parser.add_argument(
        '-f',
        '--file',
        action = 'append',
        help="Save to file"
    )
    
    get_captured_symbols_parser.add_argument(
        '-f',
        '--file',
        action = 'append',
        help="Save to file"
    )
    
    capture_next_symbol_parser.add_argument(
        '-c',
        '--channel',
        type=int,
        help="Channel (0 or 1)"
    )

    capture_symbols_parser.add_argument(
        '-c',
        '--channel',
        type=int,
        help="Channel (0 or 1)"
    )

    capture_symbols_parser.add_argument(
        '-l',
        '--list',
        type=list,
        help="list of multiple symbols (e.g. [0,1,2,3])"
    )
    
    get_captured_symbol_parser.add_argument(
        '-k',
        '--symbol_key',
        type=str,
        help="Symbol Key (e.g. ch0sym0)"
    )
    
    @cmd2.with_argparser(get_config_parser)
    def do_get_config(self, args):
        """Return current configuration of the radio"""
        response1 = requests.get(f"{url}/get_config")
        formatted_json = json.dumps(response1.json(), indent=4)	
        print(formatted_json)

    @cmd2.with_argparser(get_phy_stats_parser)
    def do_get_phy_stats(self, args):
        """Return detailed PHY statistics of the radio"""
        response1 = requests.get(f"{url}/get_phy_stats")
        formatted_json = json.dumps(response1.json(), indent=4)
        print(formatted_json)

    @cmd2.with_argparser(get_rx_power_parser)
    def do_get_rx_power(self, args):
        """Return Rx Power in dBFs and dBm along with current Rx Gains"""
        response1 = requests.get(f"{url}/get_rx_power")
        formatted_json = json.dumps(response1.json(), indent=4)
        print(formatted_json)

    @cmd2.with_argparser(get_rx_gain_parser)
    def do_get_rx_gain(self, args):
        """Return Rx Gain in dB"""
        response1 = requests.get(f"{url}/get_rx_gain")
        formatted_json = json.dumps(response1.json(), indent=4)
        print(formatted_json)

    @cmd2.with_argparser(get_tx_gain_parser)
    def do_get_tx_gain(self, args):
        """Return Tx Gain in dB"""
        response1 = requests.get(f"{url}/get_tx_gain")
        formatted_json = json.dumps(response1.json(), indent=4)
        print(formatted_json)

    @cmd2.with_argparser(get_conn_uptime_parser)
    def do_get_conn_uptime(self, args):
        """Return connetion uptime of the radio (num of seconds since active)"""
        response1 = requests.get(f"{url}/get_conn_uptime")
        print(str(int(response1.content) / 100) + " sec")

    @cmd2.with_argparser(get_conn_status_parser)
    def do_get_connection_status(self, args):
        """Return connections status of the radio"""
        response1 = requests.get(f"{url}/get_connection_status")
        print(response1.content.decode('utf-8'))

    @cmd2.with_argparser(set_close_radio_parser)
    def do_set_close_radio(self, args):
        """Disable the Radio PHY"""
        response1 = requests.get(f"{url}/set_close_radio")
        print(str(response1.content.decode('utf-8')))

    @cmd2.with_argparser(set_configure_radio_parser)
    def do_set_configure_radio(self, args):
        """Configure the Radio to start transmission"""
        response1 = requests.get(f"{url}/set_configure_radio")
        print(response1.content.decode('utf-8'))

    @cmd2.with_argparser(set_tx_gain_parser)
    def do_set_tx_gain(self, args):
        """Set Tx Gain in dB"""
        gain = args.txgain
        arg_send = {
        'gain': gain,
        }
        output = requests.post(f"{url}/set_tx_gain", json=arg_send, headers= {
                                          'Content-Type': 'application/json'})
        print(output.content.decode('utf-8'))

    @cmd2.with_argparser(set_rx_gain_parser)
    def do_set_rx_gain(self, args):
        """Set Rx Gain in dB"""
        gain = args.rxgain
        arg_send = {
        'gain': gain,
        }
        output = requests.post(f"{url}/set_rx_gain", json=arg_send, headers= {
                                          'Content-Type': 'application/json'})
        print(output.content.decode('utf-8'))

    @cmd2.with_argparser(get_captured_symbol_keys_parser)
    def do_get_captured_symbol_keys(self, args):
        """Return Symbol Keys of Captured Symbols"""
        response1 = requests.get(f"{url}/get_captured_symbol_keys")
        print(response1.content.decode('utf-8'))

    @cmd2.with_argparser(get_captured_symbols_parser)
    def do_get_captured_symbols(self, args):
        """Return Information about Captured Symbols"""
        response1 = requests.get(f"{url}/get_captured_symbols")
        print(response1.content.decode('utf-8'))

    @cmd2.with_argparser(capture_next_symbol_parser)
    def do_capture_next_symbol(self, args):
        """Capture Next Symbol"""
        ch = args.channel
        arg_send = {
        'channel': ch,
        }
        response1 = requests.post(f"{url}/capture_next_symbol", json=arg_send, headers= {
                                          'Content-Type': 'application/json'})
        print(response1.content.decode('utf-8'))

    @cmd2.with_argparser(capture_symbols_parser)
    def do_capture_symbols(self, args):
        """Capture Multiple Symbols"""
        ch = args.channel
        list = args.list
        arg_send = {
        'channel': ch,
        'list': list,
        }
        response1 = requests.post(f"{url}/capture_symbols", json=arg_send, headers= {
                                          'Content-Type': 'application/json'})
        print(response1.content.decode('utf-8'))

    @cmd2.with_argparser(get_captured_symbol_parser)
    def do_get_captured_symbol(self, args):
        """Return Captured Symbol Data in base64 format"""
        key = args.symbol_key
        arg_send = {
        'symbol_key': key,
        }
        response1 = requests.post(f"{url}/get_captured_symbol", json=arg_send, headers= {
                                          'Content-Type': 'application/json'})
        print(response1.content.decode('utf-8'))

if __name__ == '__main__':
    prompt = Prompt()
    prompt.prompt = "ara-sklk-sh> "
    prompt.cmdloop()
