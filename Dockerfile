# Use official Ubuntu 20.04 base image
FROM arawirelesshub/aramimo-base

# Update and upgrade Ubuntu
RUN apt-get update 
#&& apt-get upgrade

WORKDIR /root
# Create aramimo directory to save the scripts and CLI in
RUN mkdir aramimo

# Copy script and CLI files to the container
COPY . aramimo

# Copy cli file to usr/local/bin
COPY ara-sklk-cli.py /usr/local/bin/ara-sklk-cli

# Install git
RUN apt-get install -y python3-pip git

# Install Weather API
RUN pip install git+https://gitlab.com/aralab/arasoft/arawireless.git

# # Clone the git repository to /opt directory
# RUN git clone https://gitlab.com/aralab/arasoft/skylark/ara-sklk-api-wrapper.git

# # Change the working directory to packages
# WORKDIR ara-sklk-api-wrapper/packages

# # Install the .deb packages under /tmp/
# COPY *.deb /tmp/
# RUN dpkg -i /tmp/*.deb && apt-get install -f -y

# clone MIMMO repo (launches sklk-cli)
# run sklk-cli

# Specify the command to run when a container from this image starts 
CMD ["/bin/bash"]

