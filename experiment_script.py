#!/usr/bin/python3


"""
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import os
import sys
import time
import json
from datetime import datetime
from araweather import weather
import subprocess
import getpass
from sklk_sonic.client import JSONZClient
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

def extract_values_by_key(data, key):
    # Extract values from the data dictionaries by the given key
    values = []
    for sample in data.values():
        if isinstance(sample, dict):
            # If sample is a dictionary, get the value for the given key
            value = sample.get(key)
            if value is not None:
                values.append(value)
    return values

def extract_values_by_key_in_weather_data(data, key):
    # Initialize an empty list to store the values
    values = []

    # Iterate over each sample in the main dictionary
    for sample in data.values():
        # Check if the sample contains a 'weather_data' key
        if 'weather_data' in sample:
            # Iterate over each weather data dictionary
            for weather_data in sample['weather_data'].values():
                # Extract the value for the given key and append it to the list
                value = weather_data.get(key)
                if value is not None:
                    values.append(value)

    return values

def extract_bus_rates(data):
    # Extract bus rate data from the data dictionaries
    bus_rate_aggregate_values = []
    for sample in data.values():
        if isinstance(sample, dict):
            # If sample is a dictionary, directly get the 'bus_rate_aggregate' value
            bus_rate_aggregate_values.append(sample.get('bus_rate_aggregate'))

    return bus_rate_aggregate_values

def extract_time_stamps(data):
    # Extract time stamps from data dictionaries
    sample_times = []
    for sample in data.values():
        # Skip if sample is not a dictionary
        if not isinstance(sample, dict):
            continue

        ts_substr = sample["time_stamp"].split("_")[-3:]
        ts_substr = "_".join(ts_substr)
        time_obj = datetime.strptime(ts_substr, "%H_%M_%S")
        formatted_time = time_obj.strftime("%H:%M:%S")
        sample_times.append(formatted_time)

    return sample_times


def plot_bus_rates_vs_time(sample_times, bus_rates):
    # Create bus rate vs time graph
    plt.grid(True)
    plt.plot(sample_times, bus_rates, color='maroon', marker='o')
    plt.gca().xaxis.set_major_formatter(plt.FixedFormatter(sample_times))
    plt.gcf().autofmt_xdate()
    plt.xlabel('time')
    plt.ylabel('bus rate agg.')

    # Save the figure to a file
    plt.savefig(f"{FIG_DIR}/bus_rates_vs_time.png", dpi=300)

    plt.close()

def plot_weather_vs_bus_rates(weather_data, bus_rates, file_name):
    # Create a plot of weather data vs bus rates
    plt.grid(True)
    plt.plot(weather_data, bus_rates, color='maroon', marker='o')
    plt.xlabel('Weather Data')
    plt.ylabel('Bus Rates')

    # Save the figure to a file
    plt.savefig(f"{FIG_DIR}/" + file_name, dpi=300)

    plt.close()

# Port number
PORT = 5556

# Connect to CU
client = JSONZClient("Mu2 Control", url="tcp://10.189.0.134:" + str(PORT))

# Timestamp for file
ts_file = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")

# Initialize
data = {}
reps = int(sys.argv[1])

# Directory where the file is stored
PARENT_DIR = f"/home/{getpass.getuser()}"
print(PARENT_DIR)

EXP_DIR = f"{PARENT_DIR}/experiment_data"
NEW_EXP_DIR = f"{EXP_DIR}/" + ts_file
FIG_DIR = f"{NEW_EXP_DIR}/figures"
DATA_DIR = f"{NEW_EXP_DIR}/data"
print(EXP_DIR)

# Create direcory if not, to save the experiment data
if not os.path.exists(EXP_DIR):
    os.makedirs(EXP_DIR)

# Create these directories every time the script is run
os.makedirs(NEW_EXP_DIR)
os.makedirs(FIG_DIR)
os.makedirs(DATA_DIR)

# Save connected UEs serials and IPs
serials = []
for ue in client.ue_list():
    if (ue['connected']):
        serials.append(ue['serial'])

# Retrieve Frequency and BW in operation
config = client.get_config()
bandwidth = config[1]["radio_config"]["filter_bw"]
frequency = config[1]["radio_config"]["frequency"]

# Collect data and store in a dictionary
for i in range(reps):
    print(f"running iteration: {i+1}")
    kpis = client.gyro_get_kpis(False)
    client_stats = client.gyro_get_client_stats()
    ts = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
    data_temp = {}
    data_temp["time_stamp"] = ts

    data_temp["bandwidth"] = bandwidth
    data_temp["frequency"] = frequency

    data_temp["bus_rate_aggregate"] = kpis[0]["aggregate"]["bus"]["avg"]
    data_temp["bus_rate_dl"] = kpis[0]["downlink"]["bus"]["avg"]
    data_temp["bus_rate_ul"] = kpis[0]["uplink"]["bus"]["avg"]

    data_temp["spectral_efficiency"] = kpis[0]["aggregate"]["bus"]["avg"] / bandwidth

    data_temp["siso_avg_aggregate"] = kpis[0]["aggregate"]["bus"]["siso_avg"]
    data_temp["siso_avg_dl"] = kpis[0]["downlink"]["bus"]["siso_avg"]
    data_temp["siso_avg_ul"] = kpis[0]["uplink"]["bus"]["siso_avg"]

    data_temp["mimo_avg_aggregate"] = kpis[0]["aggregate"]["bus"]["mimo_avg"]
    data_temp["mimo_avg_dl"] = kpis[0]["downlink"]["bus"]["mimo_avg"]
    data_temp["mimo_avg_ul"] = kpis[0]["uplink"]["bus"]["mimo_avg"]

    data_temp["per_user_avg_aggregate"] = kpis[0]["aggregate"]["bus"]["per_user_avg"]
    data_temp["per_user_avg_dl"] = kpis[0]["downlink"]["bus"]["per_user_avg"]
    data_temp["per_user_avg_ul"] = kpis[0]["uplink"]["bus"]["per_user_avg"]

    data_temp["per_user_max_aggregate"] = kpis[0]["aggregate"]["bus"]["per_user_max"]
    data_temp["per_user_max_dl"] = kpis[0]["downlink"]["bus"]["per_user_max"]
    data_temp["per_user_max_ul"] = kpis[0]["uplink"]["bus"]["per_user_max"]

    data_temp["per_user_min_aggregate"] = kpis[0]["aggregate"]["bus"]["per_user_min"]
    data_temp["per_user_min_dl"] = kpis[0]["downlink"]["bus"]["per_user_min"]
    data_temp["per_user_min_ul"] = kpis[0]["uplink"]["bus"]["per_user_min"]

    for j in range(len(serials)):
        data_temp[f"dl_snr_0_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['snr_0']
        data_temp[f"dl_snr_1_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['snr_1']
        data_temp[f"ul_snr_0_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['snr_0']
        data_temp[f"ul_snr_1_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['snr_1']
        data_temp[f"dl_bus_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['bus']
        data_temp[f"ul_bus_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['bus']
        data_temp[f"dl_decode_0_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['decode_0']
        data_temp[f"dl_decode_1_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['decode_1']
        data_temp[f"ul_decode_0_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['decode_0']
        data_temp[f"ul_decode_1_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['decode_1']
        data_temp[f"dl_mcs_0_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['mcs_0']
        data_temp[f"dl_mcs_1_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['mcs_1']
        data_temp[f"ul_mcs_0_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['mcs_0']
        data_temp[f"ul_mcs_1_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['mcs_1']

    data_temp["weather_data"] = weather_data = weather.get_current_weather(['WilsonHall'])
    data[f"sample_{i+1}"] = data_temp
    # Wait for 1 second (Can be increased to collect data at lower intervals)
    time.sleep(1)

# Save to file
print("Saving Data")
data["connections"] = client.gyro_get_connections()
data_json = json.dumps(data, indent=4)
with open(f'{DATA_DIR}/experiment_{frequency}_{bandwidth}_{ts_file}.json', 'w') as fp:
    fp.write(data_json)

# Extract relevant data for plots
sample_times = extract_time_stamps(data)
bus_rates = extract_values_by_key(data, "bus_rate_aggregate")

# Extract weather data
temperatures = extract_values_by_key_in_weather_data(data, "Temperature")
humidities = extract_values_by_key_in_weather_data(data, "Humidity")
wind_speeds = extract_values_by_key_in_weather_data(data, "WindSpeed")
rain_rates = extract_values_by_key_in_weather_data(data, "RainRate")
rain_intensities = extract_values_by_key_in_weather_data(data, "RainIntensity")
snow_intensities = extract_values_by_key_in_weather_data(data, "SnowDepthIntensity")

# Create plots
plot_bus_rates_vs_time(sample_times, bus_rates)
plot_weather_vs_bus_rates(temperatures, bus_rates, "bus_rates_vs_temperature")
plot_weather_vs_bus_rates(humidities, bus_rates, "bus_rates_vs_humidity")
plot_weather_vs_bus_rates(wind_speeds, bus_rates, "bus_rates_vs_wind_speed")
plot_weather_vs_bus_rates(rain_rates, bus_rates, "bus_rates_vs_rain_rates")
plot_weather_vs_bus_rates(rain_intensities, bus_rates, "bus_rates_vs_rain_intensity")
plot_weather_vs_bus_rates(snow_intensities, bus_rates, "bus_rates_vs_snow_intensity")
