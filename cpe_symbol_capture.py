import argparse
import base64
import sys
import time

import numpy as np
import matplotlib.pyplot as plt

from flco.client import FalcoClient

def start_frame_analysis(args):
    if args.capture:
        channel = 0
        capture_symbols(args.serial, channel)
    else:
        load_symbols(args.input_dir)

def capture_symbols(serial, channel):
    print("Starting data capture mode")
    client = get_falco_client(serial)
    frame_symbols_object = generate_symbol_object(0, 139)
    # print(frame_symbols_object)
    capture_symbols_and_save_to_file(client, channel, frame_symbols_object)

def get_falco_client(serial):
    client = FalcoClient(
        ue_serial=serial,
        timeout_ms=30e3,
        wait_for_alive=False,
    )
    time.sleep(1)
    return client

def generate_symbol_object(first_symno, last_symno):
    symbols_object = []
    for idx in range(first_symno, last_symno+1):
        symbols_object.append({"symno": idx})
    return symbols_object

def capture_symbols_and_save_to_file(client, channel, symbols_object):
    key_list = client.phy_capture_symbols(channel, symbols_object)
    time.sleep(2)
    all_symbols = capture_one_symbol_and_return_complex(key_list[0], client)
    for key in key_list[1:]:
        symbol = capture_one_symbol_and_return_complex(key, client)
        # np.save(f'/tmp/symbols/{key}.npy', symbol)
        all_symbols = np.vstack((all_symbols, symbol))
    print("all_symbols: ", all_symbols.shape)
    np.save("/tmp/symbols/all_symbols.npy", all_symbols)

def capture_one_symbol_and_return_complex(key, client):
    symbol_b64 = client.phy_get_captured_symbol(key)
    symbol = np.frombuffer(
        base64.decodebytes(
            bytearray(symbol_b64, encoding='utf-8')
        ),
        dtype=np.complex64,
    )
    return symbol

def load_symbols(input_dir):
    print("Starting file input mode: ", input_dir)
    #TODO: implement loading from file


def main(sys_args):
    parser = argparse.ArgumentParser(
        description="Script to support experiments on symbol/frame capture and analysis.",
        epilog="This scrip can be used to capture symbol data, dump data to file,"
            "load information from file, and run analysis on that data.",
    )
    mode_group = parser.add_argument_group(
        "Operation mode selection",
        "Selects the operation mode between data capture or load from files",
    )
    mode_mutex = mode_group.add_mutually_exclusive_group(required=True)
    mode_mutex.add_argument(
        "-c",
        "--capture",
        action="store_true",
        help="start script in data capture mode",
    )
    mode_mutex.add_argument(
        "-i",
        "--input-dir",
        metavar="<PATH_TO_DATA>",
        help="reads input symbol from data files at given directory",
    )
    options_group = parser.add_argument_group(
        "Modifyers:",
        "Additional arguments to modify script operation",
    )
    options_group.add_argument(
        "-s",
        "--serial",
        metavar="<FLXXXXXXX>",
        help="select UEs in experiment by serial number",
    )
    options_group.add_argument(
        "-o",
        "--output-dir",
        metavar="<PATH_TO_DUMP>",
        default=".data/",
        help="directory to save captured data files",
    )
    options_group.add_argument(
        "--plot-energy",
        action="store_true",
        help="generate and save plot of energy over entire frame capture"
    )
    args = parser.parse_args(sys_args)

    # print(args)
    start_frame_analysis(args)

def sanity_check():
    print("Plotting equalized symbol 111 without CFO correction:")
    symbol_ref = np.load("./symbol98.npy")
    symbol_data = np.load("./symbol111.npy")
    print(f'Ref symbol lenght: {len(symbol_ref)}')
    print(f'Data symbol length: {len(symbol_data)}')

    data_slice = symbol_data[12+36:12+36*4]/symbol_ref[12+36:12+36*4]
    print(f'Length of data_slice for plot: {len(data_slice)}')

    plt.plot(np.imag(data_slice), np.real(data_slice), '.')
    plt.show()

if __name__ == "__main__":
    main(sys.argv[1:])
    # sanity_check()