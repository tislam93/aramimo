#!/usr/bin/python3

"""
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


# This script records csi dump and save a zip file containing three files. \\
# csi.h5 contains the actual IQ data, csi_info.json provides other important info about the CSI \\
# while hardware.json provides information about the system hardware runtime values

# To Run the Script
# ./csi_script.py <time in seconds>
# Note that the time can not be greater than 100 seconds


import os
import sys
from datetime import datetime
import requests
import getpass
import zipfile
from sklk_sonic.client import JSONZClient

# Port number
PORT = 5556

# Connect to CU
client = JSONZClient("Mu2 Control", url="tcp://10.189.0.134:" + str(PORT))

# Retrieve Frequency and BW in operation
config = client.get_config()
bandwidth = config[1]["radio_config"]["filter_bw"]
frequency = config[1]["radio_config"]["frequency"]

# Define the directory where data is saved
PARENT_DIR = f"/home/{getpass.getuser()}/sklk_data"
if not os.path.exists(PARENT_DIR):
    os.makedirs(PARENT_DIR)
CSI_DIR = f"{PARENT_DIR}/csi_data"

# Create direcory if not, to save the csi data
if not os.path.exists(CSI_DIR):
    os.makedirs(CSI_DIR)

# Generate timestamp
ts = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")

# Collect CSI data
url = 'http://10.189.0.134:8000/csi_dump'
csi_time = int(sys.argv[1])
data = {
    'csi_time': csi_time
}
response = requests.post(url, json=data)
if response.headers.get('content-type') == 'application/zip':
    # Response is a ZIP archive, save it locally
    with open(f'{CSI_DIR}/csi_{frequency}_{bandwidth}_{ts}.zip', 'wb') as file:
        file.write(response.content)

    # Extract the files from the ZIP archive
    with zipfile.ZipFile(f'{CSI_DIR}/csi_{frequency}_{bandwidth}_{ts}.zip', 'r') as zip_file:
        zip_file.extractall(f'{CSI_DIR}')

# Save files
print("Saving CSI Files!")
os.rename(f"{CSI_DIR}/csi.h5",
          f"{CSI_DIR}/csi_{frequency}_{bandwidth}_{ts}.h5")
os.rename(f"{CSI_DIR}/csi_info.json",
          f"{CSI_DIR}/csi_info_{frequency}_{bandwidth}_{ts}.json")
os.rename(f"{CSI_DIR}/hardware.json",
          f"{CSI_DIR}/hw_{frequency}_{bandwidth}_{ts}.json")

print(f"CSI Data Saved in {CSI_DIR}")